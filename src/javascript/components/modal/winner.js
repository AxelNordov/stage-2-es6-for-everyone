import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  const winnerInfo = createElement({ tagName: 'div' });
  winnerInfo.setAttribute("style", "text-align: center; color:red; font-weight: bold; padding: 20px");
  winnerInfo.innerText = `${fighter.name}`;
  showModal({ title: 'Winner', bodyElement: winnerInfo, onClose: () => { window.location.reload() } });
}
