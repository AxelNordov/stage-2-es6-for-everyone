import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {

    const TIME_FOR_NEXT_CRITICAL_HIT = 10000;

    let playerOne = {
      initHealth: firstFighter.health,
      ...firstFighter,
      inAction: false,
      blocked: false,
      lastCriticalHit: 0,
      attackControl: controls.PlayerOneAttack,
      blockControl: controls.PlayerOneBlock,
      criticalHitCombination: controls.PlayerOneCriticalHitCombination,
      indicator: document.getElementById('left-fighter-indicator')
    };

    let playerTwo = {
      initHealth: secondFighter.health,
      ...secondFighter,
      inAction: false,
      blocked: false,
      lastCriticalHit: 0,
      attackControl: controls.PlayerTwoAttack,
      blockControl: controls.PlayerTwoBlock,
      criticalHitCombination: controls.PlayerTwoCriticalHitCombination,
      indicator: document.getElementById('right-fighter-indicator')
    };

    function isIdle(player) {
      return !player.inAction && !player.blocked;
    }

    let pressed = new Set();

    document.addEventListener('keydown', function (event) {
      pressed.add(event.code);

      switch (event.code) {
        case playerOne.attackControl:
          if (isIdle(playerOne)) {
            normalAttack(playerOne, playerTwo);
          }
          break;
        case playerTwo.attackControl:
          if (isIdle(playerTwo)) {
            normalAttack(playerTwo, playerOne);
          }
          break;
        case playerOne.blockControl:
          if (isIdle(playerOne)) {
            playerOne.blocked = true;
          }
          break;
        case playerTwo.blockControl:
          if (isIdle(playerTwo)) {
            playerTwo.blocked = true;
            break;
          }
      }

      if (playerOne.criticalHitCombination.includes(event.code)) {
        criticalHitAttack(playerOne, playerTwo);
      }
      if (playerTwo.criticalHitCombination.includes(event.code)) {
        criticalHitAttack(playerTwo, playerOne);
      }

      if (playerOne.health == 0) {
        resolve(secondFighter);
      } else if (playerTwo.health == 0) {
        resolve(firstFighter);
      }
    });

    document.addEventListener('keyup', function (event) {
      switch (event.code) {
        case playerOne.attackControl:
          playerOne.inAction = false;
          break;
        case playerTwo.attackControl:
          playerTwo.inAction = false;
          break;
        case playerOne.blockControl:
          playerOne.blocked = false;
          break;
        case playerTwo.blockControl:
          playerTwo.blocked = false;
          break;
      }
      pressed.delete(event.code);
    });

    function normalAttack(attacker, enemy) {
      attacker.inAction = true;
      if (!enemy.blocked) {
        enemy.health = Math.max(0, enemy.health - getDamage(attacker, enemy))
        updateIndicator(enemy);
      }
    }

    function criticalHitAttack(attacker, enemy) {
      if (new Date().getTime() - attacker.lastCriticalHit >= TIME_FOR_NEXT_CRITICAL_HIT) {
        let criticalHit = true;
        for (let code of attacker.criticalHitCombination) {
          if (!pressed.has(code)) {
            criticalHit = false;
            break;
          }
        }
        if (criticalHit) {
          attacker.lastCriticalHit = new Date().getTime();
          enemy.health = Math.max(0, enemy.health - attacker.attack * 2);
          updateIndicator(enemy);
        }
      }
    }

    function updateIndicator(player) {
      player.indicator.style.width = (100 * player.health) / player.initHealth + "%";
    }

  });
}

export function getDamage(attacker, defender) {
  return Math.max(0, getHitPower(attacker) - getBlockPower(defender));
}

export function getHitPower(fighter) {
  return fighter.attack * chance();
}

export function getBlockPower(fighter) {
  return fighter.defense * chance();
}

function chance() {
  return Math.random() + 1;
}
