import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  fighterElement.append(createFighterImage(fighter));

  const fighterInfo = createElement({ tagName: 'div' });
  fighterInfo.setAttribute("style", "width: 100%; text-align: center; color:white;");
  fighterInfo.innerHTML = `
      <h2> ${fighter.name} </h1>
      <div> Health: <b>${fighter.health}</b> </div>
      <div> Attack: <b>${fighter.attack}</b> </div>
      <div> Defense: <b>${fighter.defense}</b> </div>
    `;
  fighterElement.append(fighterInfo);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
